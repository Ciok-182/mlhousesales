//
//  ViewController.swift
//  HouseSales
//
//  Created by Jorge Encinas on 11/04/20.
//  Copyright © 2020 Jorge Encinas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //getPrice(banios: 2, cuartos: 4, piesCuadrados: 1200)
    }
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var bedroomsLabel: UILabel!
    @IBOutlet weak var bathroomsLabel: UILabel!
    @IBOutlet weak var sqfeetLabel: UILabel!
    
    
    @IBOutlet weak var bedrooms: UISlider!
    @IBOutlet weak var bathrooms: UISlider!
    @IBOutlet weak var squareFeet: UISlider!
    
    
    
    @IBAction func valueChange(_ sender: UISlider) {
        let bedrooms = Int(self.bedrooms.value)
        let bathrooms = Int(self.bathrooms.value)
        let squareFeet = Int(self.squareFeet.value)
        
        
        print("bedrooms: \(bedrooms)")
        print("bathrooms: \(bathrooms)")
        print("squareFeet: \(squareFeet)")
        
        let a = getPrice(banios: Double(bathrooms), cuartos: Double(bedrooms), piesCuadrados: Double(squareFeet))
        print("Price: \(a)")
        priceLabel.text = "Costo de propiedad: $\(Int(a))"
        bedroomsLabel.text = "Número de dormitorios: \(bedrooms)"
        bathroomsLabel.text = "Número de baños: \(bathrooms)"
        sqfeetLabel.text = "Pies cuadrados: \(squareFeet)"
    }
    
    
   
    
    
    func getPrice (banios: Double, cuartos: Double, piesCuadrados: Double) -> Double {
        let houseSales = HouseSalesInCA()
        
        guard let prediction = try? houseSales.prediction(Bedrooms: cuartos, Bathrooms: banios, Size: piesCuadrados) else {
            print("Ha ocurrido un error")
            return 0.0
        }
        print("Precio: \(prediction.Price)")
        
        return prediction.Price
    }


}

